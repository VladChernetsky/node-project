var checkAuth = require('middleware/checkAuth');

module.exports = function(app) {

    app.get('/', require('./frontpage').get);

    app.get('/login', require('./login').get);

    app.post('/login', require('./login').post);

    app.get('/registration', require('./registration').get);

    app.post('/registration', require('./registration').post);

    app.post('/logout', require('./logout').post);

    app.get('/chat', checkAuth, require('./chat').get);

    app.get('/rooms', checkAuth, require('./rooms').get);

    app.post('/createRoom', checkAuth, require('./startgame').createRoom);

    app.get('/startgame', checkAuth, require('./startgame').get);

    app.get('/room/:id', checkAuth, require('./rooms').connectToRoom);

    app.get('/game', checkAuth, require('./game').get);

    app.get('/check', checkAuth, require('./rooms').check);

};