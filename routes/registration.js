var User = require('models/user').User;
var HttpError = require('error').HttpError;

exports.get = function(req, res) {
    res.render('registration');
};

exports.post = function(req, res, next) {
    var email = req.body.email;
    var username = req.body.username;
    var password = req.body.password;

    User.createAccount(email, username, password, function(err, user){
        if(err) {
            console.log(err);
            return next(err);
        }
        req.session.user = user._id;
        res.send({});
    });
};