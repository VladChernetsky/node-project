var Room = require('models/room').Room;
var log = require('libs/log')(module);

exports.get = function (req, res, next) {
    res.render('startgame');
};

exports.createRoom = function (req, res, next) {

    var roomName = req.body.roomName;
    var password = '';
    if(req.body.password)
        password = req.body.password;
    Room.createRoom(roomName, password, function (err, room) {
        if (err) return next(err);
        res.send({room: room});
    });
};
