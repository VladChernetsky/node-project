var Room = require('models/room').Room;
var log = require('libs/log')(module);

exports.get = function(req, res, next){

    Room.findAll(function(err, list){
        if(err) next(err);

        res.render('rooms', {
            list: list
        });
    });
};

exports.check = function(req, res){

    var roomId = req.query.id;
    var password = req.query.password;

    Room.findById(roomId, function(err, room){
        if(err) return next(err);

        if(room){
            if(room.password.toString() == password.toString()){
                res.send({response: true});
            }else{
                res.send({response: false});
            }
        }
    });
};

exports.connectToRoom = function(req, res){

    req.session.room = req.params.id;

    Room.joinToRoom(req.params.id, function(err, room){
        if(err) next(err);
        if(room == -1){
            var redirectUrl = '/rooms';
            res.redirect(redirectUrl)
        }

        res.render('room', {
            roomId: req.params.id
        });
    });
};

exports.createRoom = function(req, res, next) {

    var roomName = req.body.roomName;

    Room.createRoom(roomName, function(err, room){
        if(err) return next(err);
        res.send({room: room});
    });
};
