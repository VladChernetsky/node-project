var log = require('libs/log')(module);
var config = require('config');
var async = require('async');
var cookie = require('cookie');
var cookieParser = require('cookie-parser');
var sessionStore = require('libs/sessionStore');
var HttpError = require('error').HttpError;
var User = require('models/user').User;
var Room = require('models/room').Room;
var Player = require('models/player').Player;

var gameSocket;
var io;
var i = 0;
var players = [];
var enemies = [];

function loadSession(sid, callback) {

    sessionStore.load(sid, function (err, session) {
        if (arguments.length == 0) {
            // no arguments => no session
            return callback(null, null);
        } else {
            return callback(null, session);
        }
    });
}

function loadRoom(roomId, callback){

    Room.findById(roomId, function(err, room){
        if(err) callback(err);

        if(!room){
            return callback(null, null);
        }
        callback(null, room)
    });
}

function loadUser(session, callback) {

    if (!session.user) {
        log.debug("Session %s is anonymous", session.id);
        return callback(null, null);
    }

    User.findById(session.user, function (err, user) {
        if (err) return callback(err);

        if (!user) {
            return callback(null, null);
        }
        callback(null, user);
    });
}

module.exports = function (server) {
    var io = require('socket.io').listen(server);
    io.set('logger', log);

    //метод, который срабатывает до метода 'connection'
    io.set('authorization', function (handshake, callback) {

        console.log('auth');

        if (handshake.headers.cookie) {

            async.waterfall([
                function (callback) {

                    handshake.cookies = cookie.parse(handshake.headers.cookie || '');  //получаем куку с идентификатором сессии
                    var sidCookie = handshake.cookies[config.get('session:key')]; // подписаный идентификатор сессии (подпись была сделана express)
                    var sid = cookieParser.signedCookie(sidCookie, config.get('session:secret'));  //снимаем подпись с идентификатора

                    loadSession(sid, callback);
                },
                function (session, callback) {

                    if (!session) {
                        callback(new HttpError(401, "No session"));
                    }

                    if(session.room){
                        loadRoom(session.room, function(err, room){
                            if(err) callback(err);
                            handshake.headers.room = room;
                        });
                    }
                    handshake.session = session;
                    loadUser(session, callback);
                },
                function (user, callback) {
                    if (!user) {
                        callback(new HttpError(403, "Anonymous session may not connect"));
                    }

                    handshake.headers.user = user;
                    callback(null);
                }

            ], function (err) {
                if (!err) {
                    return callback(null, true);
                }

                if (err instanceof HttpError) {
                    log.debug('err instanceof HttpError ');
                    return callback(null, false);
                }

                log.debug('err');
                callback(err);
            });
        }
    });

    io.sockets.on('session:reload', function (sid) {
        var clients = io.sockets.clients();

        clients.forEach(function (client) {

            log.info("client session: " + client.handshake.session);

            if (client.handshake.session.id != sid) return;

            loadSession(sid, function (err, session) {
                if (err) {
                    client.emit("error", "server error");
                    client.disconnect();
                    return;
                }

                if (!session) {
                    client.emit("logout");
                    client.disconnect();
                    return;
                }

                client.handshake.session = session;
            });
        });
    });

    io.sockets.on('connection', function (socket) {
        //var username = socket.handshake.headers.user.get('username');
        initGame(io, socket.handshake.headers.user, socket.handshake.headers.room, socket);
    });

    return io;
};

function initGame(sio, userData, roomData, socket) {

    io = sio;

    gameSocket = socket;

    gameSocket.emit('connected', {username: userData.get('username'), roomId: roomData.get('_id')});

    gameSocket.room = roomData.get('_id');

    gameSocket.username = userData.username;

    gameSocket.on('playerJoinGame', onPlayerJoinGame);

    gameSocket.on("disconnect", onPlayerDisconnect);

    gameSocket.on('startGame', onStartGame);

    gameSocket.on('movePlayer', onMovePlayer);

    gameSocket.on('onFirePlayer', onFirePlayer);

    gameSocket.on('onEnemyDead', onEnemyDead);

    gameSocket.on('onFireEnemy', onFireEnemy);

    gameSocket.on('onPlayerDead', onPlayerDead);
}

function onPlayerJoinGame(room) {

    // Присоединяемся к лобби и ожидаем подключения других пользователей
    this.join(room.gameId.toString());

    var newPlayer = new Player();
    if (findPlayersByRoom(room.gameId.toString()) == false) {
        newPlayer.setX(500);
        this.x = 500;
    } else {
        newPlayer.setX(800);
        this.x = 800;
    }

    this.y = 790;

    newPlayer.setY(790);
    newPlayer.id = this.id;
    newPlayer.setUsername(this.username);
    newPlayer.setRoom(room.gameId.toString());


    // Broadcast new player to connected socket clients
    this.broadcast.to(room.gameId.toString()).emit("playerJoinedRoom", {
        id: newPlayer.id,
        x: newPlayer.getX(),
        y: newPlayer.getY(),
        username: newPlayer.getUsername()
    });

    var clients = io.sockets.adapter.rooms[room.gameId.toString()];

    for (var client in clients) {

        //отправка уведомления конкретной комнате о подключении игрока
        if (this.id != io.sockets.connected[client].id) {
            this.emit("playerJoinedRoom", {
                id: io.sockets.connected[client].id,
                x: io.sockets.connected[client].x,
                y: io.sockets.connected[client].y,
                username: io.sockets.connected[client].username
            });
        }
    }

    this.emit("currentPlayer", {id: newPlayer.id, x: newPlayer.getX(), y: newPlayer.getY(), username: newPlayer.getUsername()});
    players.push(newPlayer);
}

function onPlayerDisconnect(){
    var _this = this;
    console.log("Player was disconnected" + _this.id);
    var removePlayer = findPlayerById(_this.id);
    players.splice(players.indexOf(removePlayer), 1);

    Room.leaveRoom(_this.room, function(err, room){
        if(!err)
            if(room != null)
                _this.broadcast.to(_this.room).emit('disconnected', {username: _this.username});
    });
}

function onMovePlayer(data){
    this.broadcast.to(data.room).emit('movePlayer', {x: data.x, y: data.y});
}

function onStartGame(data) {
    i++;
    if(i == 2){
        generateEnemies(data.room);
        io.sockets.in(data.room).emit('startGame');
        i = 0;
    }
}

function generateEnemies(roomID){

    enemies[roomID] = [];
    var enemiesList = [];
    for (var i = 0; i < 3; i++) {
        enemiesList[i] = [];
    }

    for (var j = 0; j < 3; j++){
        for (var i = 0; i < 11; i++) {
            enemiesList[j][i] = true;
        }
    }
    enemies[roomID] = enemiesList;
    return enemies;
}

function onEnemyDead(data){
    console.log(data.i, data.j);
    io.sockets.in(data.room).emit('onEnemyDead', {num1: data.i, num2: data.j});
}

function onFirePlayer(){
    this.broadcast.to(this.room).emit('onFirePlayer');
}

function onFireEnemy(data){

    var enemiesCount = 0;
    for(var i = 0; i < 3; i ++){
        for(var j = 0; j < 11; j++){
            if(enemies[data.room][i][j])
                enemiesCount++;
        }
    }
    io.sockets.in(data.room).emit('onFireEnemy', {rand: Math.floor(Math.random() * (enemiesCount + 1 - 0 )) + 0});
}

function onPlayerDead(){
    this.emit('onPlayerDead');
    this.broadcast.to(this.room).emit('onRemotePlayerDead');
}

function findPlayerById(id) {
    var i;
    for (i = 0; i < players.length; i++) {
        if (players[i].id == id)
            return players[i];
    }
    return false;
}

function findPlayersByRoom(room){
    var i;
    for (i = 0; i < players.length; i++) {
        if (players[i].getRoom() == room)
            return players[i];
    }
    return false;
}
