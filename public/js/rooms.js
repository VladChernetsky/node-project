
$(document).ready(function(){

    //variables BOF
    var openModalDialog = $('#create-room');
    var saveRoomButton = $('button#save-room');
    var roomName = $('input#room-name');
    var modal = $("#myModal");
    var selectLobby = $('#select-lobby');
    var startSoloGame = $('#solo-game');
    var privateRoomCheckBox = $("#checkBox");
    var password = $("#room-pass");
    //listener BOF
    openModalDialog.on('click', function(){
        modal.modal('show');
    });

    privateRoomCheckBox.click(function() {
        $("#room-pass").toggle(this.checked);
    });

    saveRoomButton.on('click', function(){
        if(roomName.val().length <= 4){
            roomName.val('').attr('placeholder', 'Name is very short');
        }else{

            if(password.val() != '' && password.val().length < 4){
                password.val('').attr('placeholder', 'Password is very short');
            }else if(isNaN(password.val())){
                password.val('').attr('placeholder', 'Password is not a number');
            }else{
                createRoom(roomName.val(), password.val(),  modal, redirectToCreatedLobby);
            }
        }
    });

    selectLobby.on('click', goToRoomsList);
    startSoloGame.on('click', onStartSoloGame);
});


function onStartSoloGame(){
    window.location.href = '/game';
}

function redirectToCreatedLobby(newRoom){
    var jsonObj = JSON.parse(JSON.stringify(newRoom));
    window.location.href = "/room/" + jsonObj.room._id
}

function addRoomToList(newRoom){
    var roomsList = $('.room-block ul');
    var jsonObj = JSON.parse(JSON.stringify(newRoom));
    roomsList.append(
        $("<li>").append(
            $('<a>').attr('href', '#').text(jsonObj.room.roomName)
        )
    );
    window.location.href = "/room/" + jsonObj.room._id;
}

function createRoom(roomName, password, modal, callback){

    if(password =='')
        password = -1;
    var data = {roomName: roomName, password:password};

    $.ajax({
        type: 'POST',
        url: '/createRoom',
        data: data,
        success: function(data){
            modal.modal('hide');
            callback(data);
        }
    });
}

function goToRoomsList(){
    window.location.href = '/rooms';
}