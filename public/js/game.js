var IO;

var game;


var background;
var plane;
var bullets;
var bulletsRemote;
var enemy;
var cursors;
var enemyBullets;
var enemies = [];
var enemyPlanes = [];
var enemyLifes = [];
var enemyCount = 33;
var level = 0;

var bulletTime = 0;
var enemyBulletTime = 0;
var bullet;
var isMoveEnemyRight = true;
var isLevelPassed = false;
var nextLevelTime;

var currentPlayer;
var newPlayer;
var remotePlayer;
var room;
var removeRemotePlayer = false;
var removeCurrentPlayer = false;
var killedEnemy;

var randomI = -1, randomJ = -1;

$(document).ready(function () {



    IO = {
        init: function () {
            IO.socket = io.connect();
            IO.bindEvents();
        },
        bindEvents: function () {
            IO.socket.on('connected', IO.onConnected);
            IO.socket.on('playerJoinedRoom', IO.onPlayerJoinedRoom);
            IO.socket.on('currentPlayer', IO.onCurrentPlayer);
            IO.socket.on('disconnect', IO.onPlayerDisconnect);
            IO.socket.on('startGame', startNewGame);
            IO.socket.on('movePlayer', IO.onMovePlayer);
            IO.socket.on('onFirePlayer', IO.onFirePlayer);//onFireEnemy
            IO.socket.on('onFireEnemy', IO.onFireEnemy);
            IO.socket.on('onEnemyDead', IO.onEnemyDead);
            IO.socket.on('onPlayerDead', IO.onPlayerDead);
            IO.socket.on('onRemotePlayerDead', IO.onRemotePlayerDead);
        },
        onConnected: function (data) {
            room = data.roomId;
            IO.socket.emit('playerJoinGame', {username: data.username, gameId: data.roomId});
        },
        onPlayerJoinedRoom: function (data) {
            newPlayer = new NewPlayer(data.id, data.x, data.y, data.username);
            IO.socket.emit('startGame', {room: room});
        },
        onCurrentPlayer: function (data) {
            currentPlayer = new Player(data.id, data.x, data.y, data.username);
        },
        onPlayerDisconnect: function () {
            console.log("Disconnected from socket server");
            window.location.href = "/rooms";
        },
        onFirePlayer: function () {
            fireBulletRemote(remotePlayer);
        },
        onMovePlayer: function (data) {
            remotePlayer.x = data.x;
            remotePlayer.y = data.y;
        },
        onFireEnemy: function (data) {

            var rand = data.rand;
            if (rand != 0)
                for (var j = 0; j < 3; j++) {
                    for (var i = 0; i < 11; i++) {
                        if (enemyLifes[j][i] == true)
                            rand--;
                        if (rand == 0) {
                            randomI = j;
                            randomJ = i;
                        }
                    }
                }
            else {
                randomI = randomJ = 0;
            }

            enemyBullet = enemyBullets.getFirstExists(false);
            if (enemyBullet && enemyLifes[randomI][randomJ]) {
                enemyBullet.reset(enemyPlanes[randomI][randomJ].x + 150, enemyPlanes[randomI][randomJ].y + 110);
                enemyBullet.body.velocity.y = 300;
                enemyBulletTime = game.time.now + 3000;
            }

        },
        onEnemyDead: function (data) {
            if (enemyLifes[data.num1][data.num2]) {
                enemyPlanes[data.num1][data.num2].kill();
                enemyLifes[data.num1][data.num2] = false;
                enemyCount--;
            }
        },
        onPlayerDead: function(){
            removeCurrentPlayer = true;
            plane.kill();
        },
        onRemotePlayerDead:function(){
            removeRemotePlayer = true;
            remotePlayer.kill();
        }

    };
    IO.init();

});

function Player(id, x, y, username) {
    this.id = id;
    this.y = y;
    this.x = x;
    this.username = username;
}

function NewPlayer(id, x, y, username) {
    this.id = id;
    this.y = y;
    this.x = x;
    this.username = username;
}

function startNewGame() {
    $('.spinner').hide();
    $('#spinner-text').hide();
    $('h1').hide();
    game = new Phaser.Game(1900, 950, Phaser.CANVAS, 'phaser-example', {
        preload: preload,
        create: create,
        update: update,
        render: render
    });
}

function preload() {
    game.load.image('einstein', '/images/space.png');
    game.load.image('plane', '/images/airplane.png');
    game.load.image('bullet', '/images/bullet.png');
    game.load.image('enemy', '/images/enemyPlane.png');
    game.load.image('enemyBullet', '/images/enemyBullet.png');

}

function create() {
    for (var i = 0; i < 3; i++) {
        enemyPlanes[i] = [];
        enemyLifes[i] = [];
    }
    background = game.add.tileSprite(0, 0, 1900, 950, 'einstein');

    enemy = game.add.group();
    enemy.enableBody = true;
    enemy.physicsBodyType = Phaser.Physics.ARCADE;

    bullets = game.add.group();
    bullets.enableBody = true;
    bullets.physicsBodyType = Phaser.Physics.ARCADE;

    bulletsRemote = game.add.group();
    bulletsRemote.enableBody = true;
    bulletsRemote.physicsBodyType = Phaser.Physics.ARCADE;

    enemyBullets = game.add.group();
    enemyBullets.enableBody = true;
    enemyBullets.physicsBodyType = Phaser.Physics.ARCADE;


    for (var j = 0; j < 3; j++)
        for (var i = 0; i < 11; i++) {
            enemyPlanes[j][i] = enemy.create(150 * i + 100, j * 130, 'enemy', game.rnd.integerInRange(0, 36));
            enemyPlanes[j][i].name = "enemy" + i + 3 * j;
            enemyLifes[j][i] = true;
            enemyPlanes[j][i].scale.x -= 0.2;
            enemyPlanes[j][i].scale.y -= 0.2;
            //tmp.body.immovable = true;
        }
    for (var i = 0; i < 2000; i++) {
        var b = bullets.create(0, 0, 'bullet');
        b.name = 'bullet' + i;
        b.exists = false;
        //b.visible = false;
        //b.checkWorldBounds = true;
        b.events.onOutOfBounds.add(resetBullet, this);


        var bR = bulletsRemote.create(0, 0, 'bullet');
        bR.name = 'bullet' + i;
        bR.exists = false;
        //b.visible = false;
        //b.checkWorldBounds = true;
        bR.events.onOutOfBounds.add(resetBullet, this);

    }

    for (var i = 0; i < 2000; i++) {
        var b = enemyBullets.create(0, 0, 'enemyBullet');
        b.name = 'enemyBullets' + i;
        b.exists = false;
        //b.visible = false;
        //b.checkWorldBounds = true;
        b.events.onOutOfBounds.add(resetEnemyBullet, this);
    }

    plane = game.add.sprite(currentPlayer.x, currentPlayer.y, 'plane');
    game.physics.enable(plane, Phaser.Physics.ARCADE);

    //------------------------------------------------------------------------------------------//

    ////remotePlayer = new RemotePlayer(newPlayer.id, game, newPlayer.x, newPlayer.y, newPlayer.username);
    remotePlayer = game.add.sprite(newPlayer.x, newPlayer.y, 'plane');
    game.physics.enable(remotePlayer, Phaser.Physics.ARCADE);
    //------------------------------------------------------------------------------------------//

    cursors = game.input.keyboard.createCursorKeys();
    game.input.keyboard.addKeyCapture([Phaser.Keyboard.SPACEBAR]);
}

function update() {

    game.physics.arcade.collide(plane, remotePlayer);

    game.physics.arcade.overlap(bullets, enemy, enemyDead, null, this);
    game.physics.arcade.overlap(enemyBullets, plane, playerDead, null, this);
    background.tilePosition.y += 0.5;

    if (cursors.left.isDown) {
        if (plane.x > 10)
            plane.x -= 5;
    }
    else if (cursors.right.isDown) {
        if (plane.x < 1850)
            plane.x += 5;
    }

    if (game.input.keyboard.isDown(Phaser.Keyboard.SPACEBAR)) {
        fireBullet(plane);
        IO.socket.emit('onFirePlayer');
        //enemyPlanes[0][0].kill();
    }

    IO.socket.emit("movePlayer", {room: room, x: plane.x, y: plane.y});

    if (!isLevelPassed && enemyCount == 0) {
        isLevelPassed = true;
        text = game.add.text(game.world.centerX, game.world.centerY, "- Congrats -\nLevel is passed", {
            font: "65px Arial",
            fill: "#ff0044",
            align: "center"
        });
        text.anchor.setTo(0.5, 0.5);
        nextLevelTime = game.time.now + 4000;
    }
    if (game.time.now > nextLevelTime && isLevelPassed) {
        refreshLevel();
        isLevelPassed = false;
    }


    if(removeCurrentPlayer && removeCurrentPlayer){
        //window.location.href = '/startgame';
        console.log('RemotePlayer was removed');
    }

    enemyMove();
    enemyFire();
}

function enemyFire() {

    if (enemyCount > 0) {
        if (game.time.now > enemyBulletTime) {
            IO.socket.emit('onFireEnemy', {room: room});
        }
    }
}

function GetLiveEnemyPlane() {
    var rand;
    var num1 = 0, num2 = 0;
    rand = Math.floor(Math.random() * (enemyCount + 1 - 0 )) + 0;
    if (rand != 0)
        for (var j = 0; j < 3; j++) {
            for (var i = 0; i < 11; i++) {
                if (enemyLifes[j][i] == true)
                    rand--;
                if (rand == 0) {
                    return [j, i];
                }
            }
        }
    return [0, 0];
}

function enemyMove() {
    if (isMoveEnemyRight && enemy.x < 100) {
        enemy.x += 2;
        enemyBulletsMove(2);
        if (enemy.x == 100)
            isMoveEnemyRight = !isMoveEnemyRight;
    }
    else if (!isMoveEnemyRight && enemy.x > -50) {
        enemy.x -= 2;
        enemyBulletsMove(-2);
        if (enemy.x == -50)
            isMoveEnemyRight = !isMoveEnemyRight;
    }
}

function enemyBulletsMove(x) {
    for (var j = 0; j < 3; j++)
        for (var i = 0; i < 11; i++)
            enemyPlanes[j][i].x += x;
}

function fireBullet(player) {
    if (game.time.now > bulletTime && !isLevelPassed) {
        bullet = bullets.getFirstExists(false);

        if (bullet) {
            bullet.reset(player.x + 36, player.y - 8);
            bullet.body.velocity.y = -900;
            bulletTime = game.time.now + 100;
        }
        //bullet1 = bullets.getFirstExists(false);
        //
        //if (bullet1) {
        //    bullet1.reset(player.x + 30, player.y - 8);
        //    bullet1.body.velocity.y = -900;
        //    bullet1.body.velocity.x = -200;
        //}
        //
        //bullet2 = bullets.getFirstExists(false);
        //
        //if (bullet2) {
        //    bullet2.reset(player.x + 42, player.y - 8);
        //    bullet2.body.velocity.y = -900;
        //    bullet2.body.velocity.x = 200;
        //}
        //
        //bullet3 = bullets.getFirstExists(false);
        //
        //if (bullet3) {
        //    bullet3.reset(player.x + 24, player.y - 8);
        //    bullet3.body.velocity.y = -900;
        //    bullet3.body.velocity.x = -400;
        //}
        //
        //bullet4 = bullets.getFirstExists(false);
        //
        //if (bullet4) {
        //    bullet4.reset(player.x + 52, player.y - 8);
        //    bullet4.body.velocity.y = -900;
        //    bullet4.body.velocity.x = 400;
        //}
    }
}

function fireBulletRemote(player) {
    if (game.time.now > bulletTime && !isLevelPassed) {
        bullet = bulletsRemote.getFirstExists(false);

        if (bullet) {
            bullet.reset(player.x + 36, player.y - 8);
            bullet.body.velocity.y = -900;
            bulletTime = game.time.now + 100;
        }
    }
}

function refreshLevel() {
    for (var j = 0; j < 3; j++)
        for (var i = 0; i < 11; i++) {
            console.log(level);
            enemyPlanes[j][i] = enemy.create(150 * i + 100, j * 130, 'enemy', game.rnd.integerInRange(0, 36));
            enemyPlanes[j][i].name = "enemy" + i + 3 * j;
            enemyLifes[j][i] = true;
            //tmp.body.immovable = true;
        }
    enemyCount = 33;
    level++;
    text.destroy();
}

function convertIntToTwoNumbers(intNumber, numberY, numberX) {
    var num1 = 0;
    var num2 = 0;
    do {
        if (intNumber >= numberX) {
            num1++;
            intNumber -= numberX;
        }
        else {
            num2 = intNumber;
            intNumber = 0;
        }
    } while (intNumber != 0);
    return [num1, num2];
}

function resetBullet(bullet) {
    bullet.kill();
}

function resetEnemyBullet(enemyBullet) {
    enemyBullet.kill();
}

function playerDead(plane, bullet) {
    IO.socket.emit('onPlayerDead');
}

function enemyDead(bullet, enemyPlane) {
    bullet.kill();
    var num1 = convertIntToTwoNumbers((enemyPlane.z - level * 33) - 1, 3, 11);
    var num2 = num1[1];
    num1 = num1[0];
    IO.socket.emit('onEnemyDead', {room: room, i: num1, j: num2});
}

function render() {
    //    game.debug.cameraInfo(game.camera, 500, 32);
    //    game.debug.spriteCoords(plane, 32, 32);

}