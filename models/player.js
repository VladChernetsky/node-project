
var Player = function(id, sUsername, startX, startY, roomId) {
    var x = startX,
        y = startY,
        username = sUsername,
        room = roomId,
        id;

    // Getters and setters
    var getX = function() {
        return x;
    };

    var getY = function() {
        return y;
    };

    var getUsername = function() {
        return username;
    };

    var getRoom = function() {
        return room;
    };

    var setX = function(newX) {
        x = newX;
    };

    var setY = function(newY) {
        y = newY;
    };

    var setUsername = function(newUsername) {
        username = newUsername;
    };

    var setRoom = function(newRoom) {
        room = newRoom;
    };

    // Define which variables and methods can be accessed
    return {
        getX: getX,
        getY: getY,
        getUsername:getUsername,
        getRoom: getRoom,
        setX: setX,
        setY: setY,
        setUsername: setUsername,
        setRoom:setRoom,
        id: id
    }
};

exports.Player = Player;