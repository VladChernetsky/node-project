/**
 * Created by vladch on 6/11/15.
 */

var Enemy = function(cX, cY, cName, cAlive) {
    var x = cX,
        y = cY,
        name = cName,
        alive = cAlive;


    // Define which variables and methods can be accessed
    return {
        x:x,
        y:y,
        name:name,
        alive:alive
    };
};

exports.Enemy = Enemy;