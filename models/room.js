var async = require('async');
var util = require('util');
var log = require('libs/log')(module);

var mongoose = require('libs/mongoose'),
    Schema = mongoose.Schema;

var schema = new Schema({
    roomName: {
        type: String,
        unique: false,
        required: false
    },

    countPlayersInRoom:{
        type: Number,
        unique: false,
        default: 0
    },
    maxCountPlayer:{
        type: Number,
        unique: false,
        default: 2
    },

    password:{
        type: Number,
        unique: false,
        required: false,
        default: ''
    },

    created: {
        type: Date,
        default: Date.now
    }
});


schema.statics.findAll = function(callback){

    var Room = this;
    Room.find({}, function(err, list){
        callback(err, list);
    });
};

schema.statics.joinToRoom = function(roomId, callback){
    var Room = this;
    Room.findById(roomId, function(err, room){
        if(room){
            if(room.countPlayersInRoom < room.maxCountPlayer){
                room.countPlayersInRoom += 1;
                room.save(function(err){
                    if(err) callback(err);
                    callback(null, room);
                });
            }else{
                callback(null, -1);
            }
        }
    });
};


schema.statics.leaveRoom = function(roomId, callback){

    var Room = this;
    Room.findById(roomId, function(err, room){
        if(err) callback(err);
        if(room){
            if(room.countPlayersInRoom != 0){
                room.countPlayersInRoom -=1;
                room.save(function(err){
                    if(err) callback(err, null);
                    if(room.countPlayersInRoom == 0)
                        room.remove();
                    callback(null, null);
                });
            }
        }
    });
};

schema.statics.createRoom = function(roomName, password, callback){

    var Room = this;
    var room = new Room({roomName: roomName, password: password});

    room.save(function(err, room){
        if(err) log.info("err: " + err);
        callback(err, room);
    });
};


exports.Room = mongoose.model('Room', schema);