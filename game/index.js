var gameSocket;
var players = [];
var io;
exports.initGame = function(sio, userData, roomData, socket){

    io = sio;
    gameSocket = socket;

    gameSocket.emit('connected', { username: userData.get('username'), roomId:roomData.get('_id') });

    gameSocket.username = userData.username;

    gameSocket.on('playerJoinGame', onPlayerJoinGame);

};

function onPlayerJoinGame(gameData){
    // Вернем идентификатор лобби (gameId) и идентификатор сокета (mySocketId)
    // в браузер клиентa
    this.emit('newGameCreated', {gameId: gameData.gameId, socketId: this.id, username: gameData.username});

    // Присоединяемся к лобби и ожидаем подключения других пользователей
    this.join(gameData.gameId.toString());

    var clients = io.sockets.adapter.rooms[gameData.gameId.toString()];

    for (var client in clients) {
        //отправка уведомления конкретной комнате о подключении игрока
        io.sockets.in(gameData.gameId.toString()).emit('playerJoinedRoom', {username: io.sockets.connected[client].username});
        //console.log('Username: ' + io.sockets.connected[client].username);
    }
}